/*!
 * froala_editor v2.2.2 (https://www.froala.com/wysiwyg-editor)
 * License https://froala.com/wysiwyg-editor/terms/
 * Copyright 2014-2016 Froala Labs
 */

(function ($) {

  'use strict';

  $.extend($.FroalaEditor.POPUP_TEMPLATES, {
    'tweet.insert': '[_EMBED_LAYER_]'
  });

  $.FroalaEditor.PLUGINS.tweet = function (editor) {

    /**
     * Show the tweet insert popup.
     */
    function showInsertPopup () {
      var $btn = editor.$tb.find('.fr-command[data-cmd="insertTweet"]');

      var $popup = editor.popups.get('tweet.insert');
      if (!$popup) $popup = _initInsertPopup();

      if (!$popup.hasClass('fr-active')) {
        editor.popups.refresh('tweet.insert');
        editor.popups.setContainer('tweet.insert', editor.$tb);

        var left = $btn.offset().left + $btn.outerWidth() / 2;
        var top = $btn.offset().top + (editor.opts.toolbarBottom ? 10 : $btn.outerHeight() - 10);
        editor.popups.show('tweet.insert', left, top, $btn.outerHeight());
      }
    }

    function _initInsertPopup (delayed) {

      var template = {
        embed_layer: '<div class="fr-tweet-embed-layer fr-layer fr-active" id="fr-tweet-embed-layer-' + editor.id + '"><div class="fr-input-line"><textarea type="text" placeholder="' + editor.language.translate('Embedded Code') + '" tabIndex="1" rows="5"></textarea></div><div class="fr-action-buttons"><button type="button" class="fr-command fr-submit" data-cmd="tweetInsertEmbed" tabIndex="2">' + editor.language.translate('Insert') + '</button></div></div>'
      };

      // Set the template in the popup.
      return editor.popups.create('tweet.insert', template);
    }


    /**
     * Insert tweet embedded object.
     */
    function insert (embedded_code) {
      // Make sure we have focus.
      editor.events.focus(true);
      editor.selection.restore();
      editor.html.insert('<div class="fr-tweet fr-dvb">' + embedded_code + '</div>');
      editor.popups.hide('tweet.insert');
      editor.events.trigger('tweet.inserted', [$tweet]);
    }

    /**
     * Insert embedded tweet.
     */
    function insertEmbed (code) {
      if (typeof code == 'undefined') {
        var $popup = editor.popups.get('tweet.insert');
        code = $popup.find('.fr-tweet-embed-layer textarea').val() || '';
      }

      if (code.length === 0) {
        editor.events.trigger('tweet.codeError', [code]);
      }
      else {
        insert(code);
      }
    }

    function _init () {
      _initInsertPopup();
    }

    return {
      _init: _init,
      showInsertPopup: showInsertPopup,
      insertEmbed: insertEmbed,
      insert: insert
    }
  }

  $.FroalaEditor.RegisterCommand('insertTweet', {
    title: 'Insert Tweet',
    undo: false,
    focus: true,
    refreshAfterCallback: false,
    popup: true,
    callback: function () {
      if (!this.popups.isVisible('tweet.insert')) {
        this.tweet.showInsertPopup();
      }
      else {
        if (this.$el.find('.fr-marker')) {
          this.events.disableBlur();
          this.selection.restore();
        }
        this.popups.hide('tweet.insert');
      }
    },
    plugin: 'tweet'
  })

  $.FroalaEditor.DefineIcon('insertTweet', {
    NAME: 'twitter'
  });

  $.FroalaEditor.DefineIcon('tweetEmbed', { NAME: 'code' });
  $.FroalaEditor.RegisterCommand('tweetEmbed', {
    title: 'Embedded Code',
    undo: false,
    focus: false,
    callback: function () {
      this.tweet.showInsertPopup('embed-layer');
    },
    refresh: function ($btn) {
      this.tweet.refreshEmbedButton($btn);
    }
  })

  $.FroalaEditor.RegisterCommand('tweetInsertEmbed', {
    undo: true,
    focus: true,
    callback: function () {
      this.tweet.insertEmbed();
    }
  })

})(jQuery);
