(function ($) {

	$.FroalaEditor.DefineIcon('buttonIcon', { NAME: 'mediaLibrary'});

	$.FroalaEditor.RegisterCommand('mediaLibrary', {
		// Button title.
		title: 'Media library',

		// Specify the icon for the button.
		// If this option is not specified, the button name will be used.
		icon: '<i class="fa fa-camera"></i>',

		// Save the button action into undo stack.
		undo: true,

		// Focus inside the editor before the callback.
		focus: true,

		// Refresh the buttons state after the callback.
		refreshAfterCallback: true,

		// Called when the button is hit.
		callback: function () {
			// The current context is the editor instance.
			this.showMediaLibrary();
		}

	});

	$.FroalaEditor.prototype.showMediaLibrary = function (cmd) {
		// Insert HTML.
		var $editor = this;
		var $library = $('#media-library-modal');

		// Save cursor position
		$editor.selection.save();

		// Start listener
		$(document.body).on('media.insertImage', function(event, data)
		{
			$library.modal('hide');
			// Restore cursor position and insert image
			$editor.selection.restore();
			$editor.image.insert(data, true);
		});

        // Start listener
        $(document.body).on('media.insertFile', function(event, html)
        {
            $library.modal('hide');
            // Restore cursor position and insert image
            $editor.selection.restore();
            $editor.html.insert(html);
        });

		// Show library modal
		$library.modal('show')

	}

})(jQuery);