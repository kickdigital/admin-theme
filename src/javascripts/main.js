// Custom scripts
$(document).ready(function () {

	$('.switch').bootstrapSwitch();

	$('.color-picker').colorpicker();

    $('.clickable').click(function(e)
    {
        var target = $(this).data('target');
        if (target)
        {
			if (e.metaKey || e.ctrlKey)
			{
				window.open(target, '_blank');
			}
			else
			{
				window.location = target;
			}

        }
    });

    $('[data-toggle="tooltip"]').tooltip();

    //When unchecking the checkbox
    $(".check-all").on('ifUnchecked', function(event) {
        var target = $(this).data('target');
        $('input[name="' + target + '"]').iCheck("uncheck");
    }).on('ifChecked', function(event) {
        var target = $(this).data('target');
        $('input[name="' + target + '"]').iCheck("check");
    });

    $('.tab-control').on('ifChecked', function(event){
        $(this).tab('show');
    });

	$("input[type='checkbox']:not(.simple):not(.switch):not(.f-popup-line input):not(.custom-control-input), input[type='radio']:not(.simple):not(.custom-control-input):not(.switch)").iCheck({
		checkboxClass: 'icheckbox_square-green',
		radioClass: 'iradio_square-green'
	});

	$('.select-article').each(function()
	{
		$(this).selectize({
			plugins: ['remove_button', 'drag_drop'],
			valueField: 'id',
			labelField: 'title',
			searchField: 'title',
			load: function(query, callback) {
				if (!query.length) return callback();
				$.ajax({
					url: '/api/content/articles/search?q=' + encodeURIComponent(query),
					type: 'GET',
					error: function() {
						callback();
					},
					success: function(data) {
						callback(data.data);
					}
				});
			}
		});
	});

    $('.select-simple').each( function( index, element )
    {
        $( this).selectize({
			plugins: ['remove_button', 'drag_drop']
		});
    });

	$('.avatar').each( function( index, element )
	{
		$(this).avatar({
			initial_weight: 200,
			initial_bg: '#f5f5f5',
			initial_fg: '#676a6c',
			initials: $(this).data('initials'),
			hash: $(this).data('hash')
		});
	});

    $('.select-tags').selectize({
        plugins: ['remove_button'],
        valueField: 'id',
        labelField: 'name',
        searchField: 'name',
        load: function(query, callback) {
            if (!query.length) return callback();
            $.ajax({
                url: '/api/tags?q=' + encodeURIComponent(query),
                type: 'GET',
                error: function() {
                    callback();
                },
                success: function(data) {
                    callback(data.data);
                }
            });
        },
        create: function(input, callback) {
            $.ajax({
                url: '/api/tags',
                method: 'post',
                dataType: 'json',
                data: { 'name': input },
                success: function (response) {
                    callback(response);
                },
                error: function (response) {
                    callback();
                }
            });
        }
    });

    $('.datepicker').datetimepicker({
        format: 'ddd, D MMM YYYY',
		extraFormats: [
			'YYYY-MM-DD HH:mm:ss',
			'YYYY-MM-DD'
		],
        icons: {
            time: 'fa fa-clock-o',
            date: 'fa fa-calendar',
            up: 'fa fa-chevron-up',
            down: 'fa fa-chevron-down',
            previous: 'fa fa-chevron-left',
            next: 'fa fa-chevron-right',
            today: 'fa fa-screenshot',
            clear: 'fa fa-times'
        }
    });

	$('.datepicker-month').datetimepicker({
		viewMode: 'months',
		format: 'MMM YYYY',
		extraFormats: [
			'YYYY-MM-DD HH:mm:ss',
			'YYYY-MM-DD'
		],
		icons: {
			time: 'fa fa-clock-o',
			date: 'fa fa-calendar',
			up: 'fa fa-chevron-up',
			down: 'fa fa-chevron-down',
			previous: 'fa fa-chevron-left',
			next: 'fa fa-chevron-right',
			today: 'fa fa-screenshot',
			clear: 'fa fa-times'
		}
	});

    $('.timepicker').datetimepicker({
        format: 'HH:mm',
		extraFormats: [
			'YYYY-MM-DD HH:mm:ss',
			'HH:mm:ss'
		],
        stepping: 5,
        icons: {
            time: 'fa fa-clock-o',
            date: 'fa fa-calendar',
            up: 'fa fa-chevron-up',
            down: 'fa fa-chevron-down',
            previous: 'fa fa-chevron-left',
            next: 'fa fa-chevron-right',
            today: 'fa fa-screenshot',
            clear: 'fa fa-times'
        }
    });

    $('.datetimepicker').datetimepicker({
        format: 'D MMM YYYY HH:mm',
		extraFormats: [
			'YYYY-MM-DD HH:mm:ss'
		],
        stepping: 1,
        sideBySide: true,
        icons: {
            time: 'fa fa-clock-o',
            date: 'fa fa-calendar',
            up: 'fa fa-chevron-up',
            down: 'fa fa-chevron-down',
            previous: 'fa fa-chevron-left',
            next: 'fa fa-chevron-right',
            today: 'fa fa-screenshot',
            clear: 'fa fa-times'
        }
    });

	$(".wrap-text").dotdotdot();

	var areas = document.getElementsByClassName('word-count');

	if (areas.length)
	{
		var area = areas[0];
		Countable.once(area, function (counter) {
			$('#word-count').html(counter.words + ' words');
		});
		Countable.live(area, function (counter) {
			$('#word-count').html(counter.words + ' words');
		});
	}

});

function resizeIframe(obj) {
	obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
}

toastr.options.closeButton = true;
toastr.options.progressBar = true;

Dropzone.options.mediaDropzone = {
	init: function() {
		this.on('queuecomplete', function(file)
		{
			toastr.success("All files uploaded");
		});
	}
};

