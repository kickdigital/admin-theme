$(document).ready(function () {

	function refreshMediaLibrary()
	{
		if (document.getElementById('media-library-dropzone'))
		{
			var mediaLibraryDropzone = new Dropzone("#media-library-dropzone", {
				url: "/api/media"
			});
			var mediaUploadUrl;
			mediaLibraryDropzone.on("success", function(file, response) {
				mediaUploadUrl = response.media_library_link;
				console.log(response.media_library_link);
			});
			mediaLibraryDropzone.on('queuecomplete', function()
			{
				loadMediaUrl(mediaUploadUrl);
			});
		}

		$('.save-to-media-library').click(function(e)
		{
			var $button = $(this);
			$button.addClass('disabled');
			$button.html('<i class="fa fa-spinner fa-pulse"></i> Saving...');
		});
	}

	function loadMediaUrl(url)
	{
		$('#media-library').load(url, function()
		{
			refreshMediaLibrary();
		});
	}

	$(document.body).on('click', '#media-library .media-link, #media-library .pagination a, #media-library-upload-link', function(event)
	{
		event.preventDefault();
		var url = $(this).attr('href');
		loadMediaUrl(url);
	});

	$(document.body).on('click', '#insert-image', function(event)
	{
		event.preventDefault();
		var imageUrl = $('input[name=size]:checked', '#image-sizes').val();

		$(this).trigger('media.insertImage', [imageUrl]);
	});

    $(document.body).on('click', '#insert-file', function(event)
    {
        event.preventDefault();
        var url = $('#file-url').val();
        var title = $('#file-title').val();
        var html = '<a href="' + url + '" target="_blank" class="fr-file">' + title + '</a>';

        $(this).trigger('media.insertFile', html);
    });

	$('#media-library-modal').on('shown.bs.modal', function(event)
	{
		loadMediaUrl('/admin/media/library');
	}).on('hidden.bs.modal', function(event)
	{
		$(document.body).off('media.insertImage');
		$(document.body).off('media.insertFile');
	});

	$('#select-featured-image').click(function(event)
	{
		$('#media-library-modal').modal('show');
		$(document.body).on('media.insertImage', function(event, data)
		{
			$('#media-library-modal').modal('hide');
			var html = '<img src="' + data + '" class="img-thumbnail img-responsive" />';
			$('#featured-image').html(html);
			$('input[name=image_url]').val(data);
		});
	});

	$('.select-image').click(function(event)
	{
		var $button = $(this);
		var target = $button.data('target') === undefined ? 'image_url' : $button.data('target');

		$('#media-library-modal').modal('show');
		$(document.body).on('media.insertImage', function(event, data)
		{
			$('#media-library-modal').modal('hide');
			var html = '<img src="' + data + '" class="img-thumbnail img-responsive" />';
			$button.parent().find('.selected-image').html(html);
			$button.parent().find('input').val(data);
		});
	});

	$('.submit-filters').on('click', function(event)
	{
		var data = $(this).closest('form').serialize();
		data = data + '&' + this.name + '=' + this.value;
		var url = '/admin/media/library?' + data;
		loadMediaUrl(url);
		event.preventDefault();
	});

	$('#media-library-upload').on('click', function(event)
	{
		loadMediaUrl('/admin/media/library/upload');
		event.preventDefault();
	});




});