(function($) {
	$('input[type=checkbox][data-children]').unbind("change childchange childchangebubble").each(function() {
		var parent = $(this);

		var linkedChildren = $(parent.data("children"));

		//trigger a "childChange" event on the parent when any child is triggered.
		linkedChildren.bind("change childchangebubble", function() {
			parent.trigger("childchange");
		});

		parent.bind({
			"change": function(event) { // Bind change event to check all children
				linkedChildren.prop("checked", parent.prop("checked")).trigger("change");
			},
			"childchange": function() { // have to bind custom event seperately, there seems to be a jQuery bug.
				// When a child is changed recalculate if parent should be checked
				var noChildrenUnchecked = !linkedChildren.is(":not(:checked)");
				parent.prop("checked", noChildrenUnchecked).trigger("childchangebubble");
			}
		});
	});
})(jQuery);