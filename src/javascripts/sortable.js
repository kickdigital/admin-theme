var el = document.getElementById('featured-articles');

if (el)
{
	var sortable = Sortable.create(el, {

		animation: 150,
		handle: '.drag-handle',

		// dragging ended
		onEnd: function (evt) {
			var item = evt.item;
			var next = item.nextSibling;
			var next_id;

			if (next !== null)
			{
				next_id = next.dataset.id;
			}
			else
			{
				next_id = next;
			}

			$.ajax({
				url: '/api/content/articles/order',
				method: 'post',
				dataType: 'json',
				data: {
					'next_id': next_id,
					'article_id': item.dataset.id,
					'position': evt.newIndex
				},
				success: function (response) {
					toastr.success(response);
				},
				error: function (response) {
					toastr.error(response);
				}
			});
		}
	});
}

